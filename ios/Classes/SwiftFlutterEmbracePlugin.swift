import Flutter
import Embrace
import UIKit

public class SwiftFlutterEmbracePlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_embrace", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterEmbracePlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }
    
    private func getArgumentsError(_ call: FlutterMethodCall) -> FlutterError {
        return FlutterError(code: "Invalid Arguments", message: "Invalid arguments for \(call.method)", details: call.arguments)
    }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    print(call.method)
    print(call.arguments)
    switch (call.method) {
    case "start":
        print("SwiftFlutterEmbracePlugin", "start: $enableIntegrationTesting")
        Embrace.sharedInstance().start()
        result(true) // success
    case "setUserIdentifier":
        guard let id = call.arguments as? String else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().setUserIdentifier(id)
        result("\(call.method), \(call.arguments)") // success
    case "clearUserIdentifier":
        Embrace.sharedInstance().clearUserIdentifier()
        result(true)
    case "setUsername":
        guard let username = call.arguments as? String else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().setUsername(username)
        result("\(call.method), \(call.arguments)")
    case "clearUsername":
        Embrace.sharedInstance().clearUsername()
        result("\(call.method), \(call.arguments)")
    case "setUserEmail":
        guard let email = call.arguments as? String else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().setUserEmail(email)
        result("\(call.method), \(call.arguments)")
    case "clearUserEmail":
        Embrace.sharedInstance().clearUserEmail()
        result("\(call.method), \(call.arguments)")
    case "setUserAsPayer":
        Embrace.sharedInstance().setUserAsPayer()
        result("\(call.method), \(call.arguments)")
    case "clearUserAsPayer":
        Embrace.sharedInstance().clearUserAsPayer()
        result("\(call.method), \(call.arguments)")
    case "setUserPersona":
        guard let persona = call.arguments as? String else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().setUserPersona(persona)
        result("\(call.method), \(call.arguments)")
    case "clearUserPersona":
        guard let persona = call.arguments as? String else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().clearUserPersona(persona)
        result("\(call.method), \(call.arguments)")
    case "clearAllUserPersonas":
        Embrace.sharedInstance().clearAllUserPersonas()
        result("\(call.method), \(call.arguments)") // success
    
    // Moments
    
    case "startEvent":
        guard let map = call.arguments as? [String:Any?] else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().startMoment(withName: map["name"] as! String, identifier: map["identifier"] as? String, allowScreenshot: map["allowScreenshot"] as? Bool ?? false, properties: map["properties"] as? [String : String])
        result("\(call.method), \(call.arguments)") // success
    
    case "endEvent":
        guard let map = call.arguments as? [String:Any?] else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().endMoment(withName: map["name"] as! String, identifier: map["identifier"] as? String, properties: map["properties"] as? [String : String])
        result("\(call.method), \(call.arguments)")
        
    //      TODO: Embrace.sharedInstance().startAppStartup()
    //      "startAppStartup" -> {
    //        result.complete {
    //          Embrace.getInstance().startAppStartup()
    //        }
    //      }
    case "endAppStartup":
        Embrace.sharedInstance().endAppStartup()
        result("\(call.method), \(call.arguments)") // success
    
    // Logs
    
    case "logInfo":
        guard let map = call.arguments as? [String:Any?] else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().logMessage(map["message"] as! String, with: .info, properties: map["properties"] as? [String:String], takeScreenshot: false)
        result("\(call.method), \(call.arguments)") // success
    case "logWarning":
        guard let map = call.arguments as? [String:Any?] else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().logMessage(map["message"] as! String, with: .warning, properties: map["properties"] as? [String:String], takeScreenshot: false)
        result("\(call.method), \(call.arguments)") // success
    case "logError":
        guard let map = call.arguments as? [String:Any?] else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().logMessage(map["message"] as! String, with: .error, properties: map["properties"] as? [String:String], takeScreenshot: map["allowScreenshot"] as? Bool ?? false)
        result("\(call.method), \(call.arguments)") // success
    case "logBreadcrumb":
        guard let message = call.arguments as? String else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().logBreadcrumb(withMessage: message)
        result("\(call.method), \(call.arguments)") // success
    case "logNetworkCall":
        guard let map = call.arguments as? [String:Any?] else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().logNetworkRequest(EMBNetworkRequest(urlString: map["url"] as! String,
                                                                     method: map["method"] as! String,
                                                                     startTime: Date(timeIntervalSince1970: (map["startTime"] as! NSNumber).doubleValue),
                                                                     endTime: Date(timeIntervalSince1970: (map["endTime"] as! NSNumber).doubleValue),
                                                                     bytesIn: (map["bytesSent"] as? NSNumber)?.intValue ?? -1,
                                                                     bytesOut: (map["bytesReceived"] as? NSNumber)?.intValue ?? -1,
                                                                     responseCode:(map["statusCode"] as? NSNumber)?.intValue ?? -1,
                                                                     error: nil,
                                                                     traceId: nil)!)
        result("\(call.method), \(call.arguments)") // success
    case "logNetworkError":
        guard let map = call.arguments as? [String:Any?] else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().logNetworkRequest(EMBNetworkRequest(urlString: map["url"] as! String,
                                                                     method: map["method"] as! String,
                                                                     startTime: Date(timeIntervalSince1970: (map["startTime"] as! NSNumber).doubleValue),
                                                                     endTime: Date(timeIntervalSince1970: (map["endTime"] as! NSNumber).doubleValue),
                                                                     bytesIn: (map["bytesSent"] as? NSNumber)?.intValue ?? -1,
                                                                     bytesOut: (map["bytesReceived"] as? NSNumber)!.intValue,
                                                                     responseCode:(map["statusCode"] as? NSNumber)?.intValue ?? -1,
                                                                     error: NSError(domain: map["errorType"] as? String ?? "Unknown", code: 0, userInfo: nil),
                                                                     traceId: map["errorMessage"] as? String)!)
        result("\(call.method), \(call.arguments)") // success
    case "logView":
        guard let name = call.arguments as? String else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().startView(withName: name)
        result("\(call.method), \(call.arguments)") // success
    case "logWebView":
        // TODO This is less than ideal
        guard let name = call.arguments as? String else {
            return result(getArgumentsError(call))
        }
        Embrace.sharedInstance().logWebViewBeganRequest(URLRequest(url: URL(string: name)!))
        result("\(call.method), \(call.arguments)") // success
    case "logTap":
//        TODO This isn't available to call from Embrace.
//        guard let name = call.arguments as? String else {
//            return result(getArgumentsError(call))
//        }
//        Embrace.sharedInstance().logTap?
        result("\(call.method), \(call.arguments)") // success
    case "crash":
        // TODO THIS IS A MAJOR FLAW HERE, no way to send up a crash :/
        //
        //      "crash" -> {
        //        result.complete(
        //                call.argumentOrNull<String>("exception"),
        //                call.argumentOrNull<List<Map<String, String>>>("stackTraceElements") ?: emptyList()
        //        ) { message, stackTraceElementMaps ->
        //          Thread {
        //            throw Exception(message).apply {
        //              stackTrace = stackTraceElementMaps.mapNotNull { stackTraceElement(it) }.toTypedArray()
        //            }
        //          }.run()
        //        }
        result("\(call.method), \(call.arguments)") // success
    
    // Misc
    
    case "endSession":
        let clearUserInfo = call.arguments as? Bool ?? false
        Embrace.sharedInstance().endSession(clearUserInfo)
        result("\(call.method), \(call.arguments)") // success
    case "stop":
//        TODO this doesn't exist for iOS
//        let clearUserInfo = call.arguments as? Bool ?? false
//        Embrace.sharedInstance().stop()
        result("\(call.method), \(call.arguments)") // success
    case "isStarted":
//        TODO This isn't on iOS?
//        guard let message = call.arguments as? String else {
//            return result(getArgumentsError(call))
//        }
//        Embrace.sharedInstance().
        result(true) // success
    
    default:
        result("\(call.method), \(call.arguments)")
    }
    
  }
}
