#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
Pod::Spec.new do |s|
  s.name             = 'flutter_embrace'
  s.version          = '0.0.2'
  s.summary          = 'Embrace SDK for Flutter'
  s.description      = <<-DESC
Embrace SDK for Flutter
                       DESC
  s.homepage         = 'http://embrace.io'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Kody Van Ry' => 'kody@pineconemobile.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.dependency 'EmbraceIO'

  s.ios.deployment_target = '8.0'
end

